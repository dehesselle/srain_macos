#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Create the Srain application bundle.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

#------------------------------------------------------ source jhb configuration

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

#------------------------------------------- source common functions from bash_d

# bash_d is already available (it's part of jhb configuration)

bash_d_include error
bash_d_include lib

#--------------------------------------------------- source additional functions

source "$(dirname "${BASH_SOURCE[0]}")"/src/srain.sh

### variables ##################################################################

SELF_DIR=$(dirname "${BASH_SOURCE[0]}")

### functions ##################################################################

# Nothing here.

### main #######################################################################

error_trace_enable

#----------------------------------------------------- create application bundle

(
  cd "$SELF_DIR" || exit 1
  export ARTIFACT_DIR   # is referenced in srain.bundle

  jhb run gtk-mac-bundler src/srain.bundle
)

#------------------------------------------------------------- update Info.plist

# enable HiDPI
/usr/libexec/PlistBuddy -c "Add NSHighResolutionCapable bool 'true'" \
  "$SRAIN_APP_PLIST"

# enable dark mode (menubar only, GTK theme is reponsible for the rest)
/usr/libexec/PlistBuddy -c "Add NSRequiresAquaSystemAppearance bool 'false'" \
  "$SRAIN_APP_PLIST"

# set minimum OS version
/usr/libexec/PlistBuddy -c "Set LSMinimumSystemVersion '$SYS_SDK_VER'" \
  "$SRAIN_APP_PLIST"

# set Srain version
/usr/libexec/PlistBuddy -c \
  "Set CFBundleShortVersionString '$(srain_get_version)'" \
  "$SRAIN_APP_PLIST"
/usr/libexec/PlistBuddy -c "Set CFBundleVersion '$SRAIN_BUILD'" \
  "$SRAIN_APP_PLIST"

# set copyright
/usr/libexec/PlistBuddy -c "Set NSHumanReadableCopyright 'Copyright © \
2022 Shengyu Zhang'" "$SRAIN_APP_PLIST"

# set app category
/usr/libexec/PlistBuddy -c "Add LSApplicationCategoryType string \
'public.app-category.social-networking'" "$SRAIN_APP_PLIST"

# set folder access descriptions
# /usr/libexec/PlistBuddy -c "Add NSDesktopFolderUsageDescription string \
# 'Srain needs your permission to access the Desktop folder.'" "$SRAIN_APP_PLIST"
# /usr/libexec/PlistBuddy -c "Add NSDocumentsFolderUsageDescription string \
# 'Srain needs your permission to access the Documents folder.'" "$SRAIN_APP_PLIST"
# /usr/libexec/PlistBuddy -c "Add NSDownloadsFolderUsageDescription string \
# 'Srain needs your permission to access the Downloads folder.'" "$SRAIN_APP_PLIST"
# /usr/libexec/PlistBuddy -c "Add NSRemoveableVolumesUsageDescription string \
# 'Srain needs your permission to access removeable volumes.'" "$SRAIN_APP_PLIST"

# add supported languages
/usr/libexec/PlistBuddy -c "Add CFBundleLocalizations array" \
  "$SRAIN_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add CFBundleLocalizations: string 'en'" \
  "$SRAIN_APP_PLIST"  # because there is no en.po file
for locale in "$SRC_DIR"/srain-*/po/*.po; do
  /usr/libexec/PlistBuddy -c "Add CFBundleLocalizations: string \
'$(basename -s .po $locale)'" "$SRAIN_APP_PLIST"
done

# add some metadata to make CI identifiable
if $CI; then
  for var in PROJECT_NAME PROJECT_URL COMMIT_BRANCH COMMIT_SHA COMMIT_SHORT_SHA\
             JOB_ID JOB_URL JOB_NAME PIPELINE_ID PIPELINE_URL; do
    # use awk to create camel case strings (e.g. PROJECT_NAME to ProjectName)
    /usr/libexec/PlistBuddy -c "Add CI$(\
      echo $var | awk -F _ '{
        for (i=1; i<=NF; i++)
        printf "%s", toupper(substr($i,1,1)) tolower(substr($i,2))
      }'
    ) string $(eval echo \$CI_$var)" "$SRAIN_APP_PLIST"
  done
fi
