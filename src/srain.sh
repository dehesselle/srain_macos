# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# This file contains settings and functions specific to Srain.

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced
# shellcheck disable=SC2034 # no exports desired

### dependencies ###############################################################

# Nothing here.

### variables ##################################################################

SRAIN_APP_DIR=$ARTIFACT_DIR/Srain.app
SRAIN_APP_CON_DIR=$SRAIN_APP_DIR/Contents
SRAIN_APP_RES_DIR=$SRAIN_APP_CON_DIR/Resources
SRAIN_APP_BIN_DIR=$SRAIN_APP_RES_DIR/bin
SRAIN_APP_ETC_DIR=$SRAIN_APP_RES_DIR/etc
SRAIN_APP_LIB_DIR=$SRAIN_APP_RES_DIR/lib
SRAIN_APP_FRA_DIR=$SRAIN_APP_CON_DIR/Frameworks
SRAIN_APP_PLIST=$SRAIN_APP_CON_DIR/Info.plist

SRAIN_BUILD=${SRAIN_BUILD:-0}

### functions ##################################################################

function srain_get_version
{
  xmllint \
    --xpath "string(//moduleset/meson[@id='srain']/branch/@version)" \
    "$SELF_DIR"/modulesets/srain.modules
}